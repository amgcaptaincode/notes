package com.test.notes.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.test.notes.app.Constants;

import static com.test.notes.app.Constants.API_KEY;

public class PrefUtils {

    public PrefUtils() {
    }

    private static SharedPreferences getSharedPref(Context context) {
        return context.getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
    }

    public static void storeApiKey(Context context, String apiKey) {
        SharedPreferences.Editor editor = getSharedPref(context).edit();
        editor.putString(API_KEY, apiKey);
        editor.commit();
    }

    public static String getApiKey(Context context) {
        return getSharedPref(context).getString(API_KEY, null);
    }
}
