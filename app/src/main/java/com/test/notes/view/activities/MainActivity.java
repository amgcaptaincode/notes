package com.test.notes.view.activities;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.test.notes.R;
import com.test.notes.network.ApiClient;
import com.test.notes.network.ApiService;
import com.test.notes.models.Note;
import com.test.notes.models.User;
import com.test.notes.presenters.MainPresenter;
import com.test.notes.presenters.MainViewInterface;
import com.test.notes.utils.MyDividerItemDecoration;
import com.test.notes.utils.PrefUtils;
import com.test.notes.utils.RecyclerTouchListener;
import com.test.notes.view.adapters.NotesAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.HttpException;

public class MainActivity extends AppCompatActivity implements MainViewInterface {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private NotesAdapter mAdapter;
    private List<Note> noteList = new ArrayList<>();

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.txt_empty_notes_view)
    TextView noNotesView;

    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupViews();
        setupPresenter();

        recyclverViewListener();

        checkStoredApiKey();

    }

    private void setupViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.activity_title_home));
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNoteDialog(false, null, -1);
            }
        });

        // white background notification bar
        whiteNotificationBar(fab);

        apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);
        mAdapter = new NotesAdapter(this, noteList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);
    }

    private void setupPresenter() {
        mainPresenter = new MainPresenter(this, getApplicationContext());
    }

    private void recyclverViewListener() {
        /*
         * On long press on RecyclerView item, open alert dialog
         * with options to chose
         * Edit and Delete
         * */
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {
                showActionsDialog(position);
            }
        }));
    }

    private void checkStoredApiKey() {
        /*
         * Check for stored Api Key in shared preferences
         * If not present, make api call to register the user
         * This will be executed when app is installed for the first time
         * or data is cleared from settings
         * */
        if (TextUtils.isEmpty(PrefUtils.getApiKey(this))) {
            registerUser();
        } else {
            // user is already register, fetch all notes
            fetchAllNotes();
        }
    }
    /**
     * Registering new user
     * sending unique id as device identification
     * https://developer.android.com/training/articles/user-data-ids.html
     */
    private void registerUser() {
        String uniqueId = UUID.randomUUID().toString();
        mainPresenter.registerUser(uniqueId);
    }

    /*
     * Fetching all notes from api
     * The received items will be in random order
     * map() operator is used to sort the items in descending order by Id
     */
    private void fetchAllNotes() {
        mainPresenter.fetchAllNotes();
    }

    /*
    * Create new note
    * */
    private void createNote(String note) {
        mainPresenter.createNote(note);
    }

    /*
    * Update a note
    * */
    public void updateNote(int noteId, final String note, final int position) {
        mainPresenter.updateNote(noteId, note, position);
    }

    /*
     * Deleting a note
     * */
    private void deleteNote(final int noteId, final int position) {
        mainPresenter.deleteNote(noteId, position);
        Log.e(TAG, "deleteNote: " + noteId + ", " + position);
    }

    /**
     * Shows alert dialog with EditText options to enter / edit
     * a note.
     * when shouldUpdate=true, it automatically displays old note and changes the
     * button text to UPDATE
     */
    private void showNoteDialog(final  boolean shouldUpdate, final Note note, final int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        View view = layoutInflater.inflate(R.layout.note_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(view);

        final EditText edNote = view.findViewById(R.id.note);
        TextView dialogTitle = view.findViewById(R.id.dialog_title);
        dialogTitle.setText(!shouldUpdate ? getString(R.string.lbl_new_note_title) : getString(R.string.lbl_edit_note_title));

        if (shouldUpdate && note != null) {
            edNote.setText(note.getNote());
        }

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(shouldUpdate ? "Update" : "Save",
                        new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        dialogBox.cancel();
                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // show toast message when no text is entered
                if (TextUtils.isEmpty(edNote.getText().toString())) {
                    showToast("Enter note");
                    return;
                } else {
                    alertDialog.dismiss();
                }

                // check if user updating note
                if (shouldUpdate && note != null) {
                    // updte note by it's id
                    updateNote(note.getId(), edNote.getText().toString(), position);
                } else {
                    createNote(edNote.getText().toString());
                }

            }
        });

    }

    /**
     * Opens dialog with Edit - Delete options
     * Edit - 0
     * Delete - 0
     */
    private void showActionsDialog(final int position) {
        CharSequence colors[] = new CharSequence[] {"Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose option");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    showNoteDialog(true, noteList.get(position), position);
                } else {
                    deleteNote(noteList.get(position).getId(), position);
                }
            }
        });
        builder.show();

    }


    private void toggleEmptyNotes() {
        if (noteList.size() > 0) {
            noNotesView.setVisibility(View.GONE);
        } else {
            noNotesView.setVisibility(View.VISIBLE);
        }
    }

    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }

    @Override
    public void saveUSerApiKey(User user) {
        // Storing user API key in preference
        PrefUtils.storeApiKey(getApplicationContext(), user.getApiKey());
        showToast("Device is registered successfully! ApiKey:"
                + PrefUtils.getApiKey(getApplicationContext()));

    }

    @Override
    public void showAllNotes(List<Note> notes) {
        noteList.clear();
        noteList.addAll(notes);
        mAdapter.notifyDataSetChanged();

        toggleEmptyNotes();
    }

    @Override
    public void noteCreated(Note note) {
        Log.d(TAG, "new note created: " + note.getId() + ", " + note.getNote() + ", " + note.getTimestamp());
        // Add new item and notify adapter
        noteList.add(0, note);
        mAdapter.notifyItemInserted(0);

        toggleEmptyNotes();
    }

    @Override
    public void noteUpdated(String note, int position) {
        Log.d(TAG, "Note updated!");
        Note n = noteList.get(position);
        n.setNote(note);

        // Update item and notify adapter
        noteList.set(position, n);
        mAdapter.notifyItemChanged(position);

        toggleEmptyNotes();
    }

    @Override
    public void noteDeleted(int position) {
        // Remove and notifty adapter about item deletion
        noteList.remove(position);
        mAdapter.notifyItemRemoved(position);

        showToast("Note deleted!");

        toggleEmptyNotes();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showError(String e) {
        Log.e(TAG, "onError" + e);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, e, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }
}
