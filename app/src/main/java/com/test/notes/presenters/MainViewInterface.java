package com.test.notes.presenters;

import com.test.notes.models.Note;
import com.test.notes.models.User;

import java.util.List;

public interface MainViewInterface {

    void saveUSerApiKey(User user);
    void showAllNotes(List<Note> notes);
    void noteCreated(Note note);
    void noteUpdated(String note, int position);
    void noteDeleted(int position);
    void showToast(String message);
    void showError(String e);




}
