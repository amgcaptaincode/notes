package com.test.notes.presenters;

import android.content.Context;

import com.test.notes.models.Note;

import java.util.List;

public interface MainPresenterInterface {

    void registerUser(String deviceID);

    void createNote(String note);

    void fetchAllNotes();

    void updateNote(int noteId, String note, int position);

    void deleteNote(int noteId, int position);

}
