package com.test.notes.presenters;

import android.content.Context;
import android.text.TextUtils;

import com.test.notes.models.Note;
import com.test.notes.models.User;
import com.test.notes.network.ApiClient;
import com.test.notes.network.ApiService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class MainPresenter implements MainPresenterInterface {

    private static final String TAG = MainPresenter.class.getSimpleName();
    private MainViewInterface mainViewInterface;
    private Context context;

    public MainPresenter(MainViewInterface mainViewInterface, Context context) {
        this.mainViewInterface = mainViewInterface;
        this.context = context;
    }

    @Override
    public void registerUser(String deviceID) {
        getObservableRegister(deviceID).subscribeWith(getObserverRegister());
    }

    private Single<User> getObservableRegister(String deviceID) {
        return ApiClient.getClient(context)
                .create(ApiService.class)
                .register(deviceID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver getObserverRegister() {
        return new DisposableSingleObserver<User>() {
            @Override
            public void onSuccess(User user) {
                mainViewInterface.saveUSerApiKey(user);
            }

            @Override
            public void onError(Throwable e) {
                mainViewInterface.showError(messageError(e));
            }
        };
    }

    @Override
    public void createNote(String note) {
        getObservableNewNote(note).subscribeWith(getObserverNewNote());
    }

    private Single<Note> getObservableNewNote(String note) {
        return ApiClient.getClient(context).create(ApiService.class).createNote(note)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver getObserverNewNote() {
        return new DisposableSingleObserver<Note>() {
            @Override
            public void onSuccess(Note note) {
                if (!TextUtils.isEmpty(note.getError())) {
                    mainViewInterface.showToast(note.getError());
                    return;
                }
                mainViewInterface.noteCreated(note);
            }

            @Override
            public void onError(Throwable e) {
                mainViewInterface.showError(messageError(e));
            }
        };
    }

    @Override
    public void fetchAllNotes() {
        getObservableFetchAllNote().subscribeWith(getObserverFetchAllNote());
    }

    private Single<List<Note>> getObservableFetchAllNote() {
        return ApiClient.getClient(context).create(ApiService.class).fetchAllNotes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<List<Note>, List<Note>>() {
                    @Override
                    public List<Note> apply(List<Note> notes) throws Exception {
                        //  TODO - note about sort
                        Collections.sort(notes, new Comparator<Note>() {
                            @Override
                            public int compare(Note n1, Note n2) {
                                return n2.getId() - n1.getId();
                            }
                        });
                        return notes;
                    }
                });
    }

    private DisposableSingleObserver getObserverFetchAllNote() {
        return new DisposableSingleObserver<List<Note>>() {
            @Override
            public void onSuccess(List<Note> notes) {
                mainViewInterface.showAllNotes(notes);
            }

            @Override
            public void onError(Throwable e) {
                mainViewInterface.showError(messageError(e));
            }
        };
    }

    @Override
    public void updateNote(int noteId, String note, int position) {
        getObservableUpdateNote(noteId, note).subscribeWith(getObserverUpdateNote(note, position));
    }

    public Completable getObservableUpdateNote(int noteId, String note) {
        return ApiClient.getClient(context).create(ApiService.class).updateNote(noteId, note)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableCompletableObserver getObserverUpdateNote(final String note, final int position) {
        return new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                mainViewInterface.noteUpdated(note, position);
            }

            @Override
            public void onError(Throwable e) {
                mainViewInterface.showError(messageError(e));
            }
        };
    }

    @Override
    public void deleteNote(int noteId, int position) {
        getObservableDeleteNote(noteId).subscribeWith(getObserverDeleteNote(position));
    }

    private Completable getObservableDeleteNote(int noteId) {
        return ApiClient.getClient(context).create(ApiService.class).deleteNote(noteId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableCompletableObserver getObserverDeleteNote(final int position) {
        return new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                mainViewInterface.noteDeleted(position);
            }

            @Override
            public void onError(Throwable e) {
                mainViewInterface.showError(messageError(e));
            }
        };
    }

    /**
     * Showing a Snackbar with error message
     * The error body will be in json format
     * {"error": "Error message!"}
     */
    private String messageError(Throwable e) {
        String message = "";
        try {
            if (e instanceof IOException) {
                message = "No internet connection!";
            } else if (e instanceof HttpException) {
                HttpException error = (HttpException) e;
                String errorBody = error.response().errorBody().string();
                JSONObject jObj = new JSONObject(errorBody);

                message = jObj.getString("error");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        if (TextUtils.isEmpty(message)) {
            message = "Unknown error occurred! Check LogCat.";
        }
        return message;
    }
}
