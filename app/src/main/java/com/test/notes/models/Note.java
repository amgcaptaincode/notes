package com.test.notes.models;

public class Note extends BaseResponse {

    private int id;
    private String note;
    private String timestamp;

    public int getId() {
        return id;
    }

    public String getNote() {
        return note;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
