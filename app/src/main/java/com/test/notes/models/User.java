package com.test.notes.models;

import com.google.gson.annotations.SerializedName;

public class User extends BaseResponse {

    @SerializedName("api_key")
    private String apiKey;

    public String getApiKey() {
        return apiKey;
    }
}
